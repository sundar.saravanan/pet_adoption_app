import 'package:dartz/dartz.dart';
import 'package:pet_adoption_app/core/error/failure.dart';
import 'package:pet_adoption_app/core/usecase/usecase.dart';

import '../entity/pet.dart';
import '../repository/pet_adoption_repository.dart';

class GetPetsList extends UseCase<List<Pet>, NoParams> {
  final PetAdoptionRepository repository;

  GetPetsList(this.repository);

  @override
  Future<Either<Failure, List<Pet>>> call(NoParams params) {
    return repository.getPetsList();
  }
}
