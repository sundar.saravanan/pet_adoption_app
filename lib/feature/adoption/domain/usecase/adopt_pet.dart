import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:pet_adoption_app/core/error/failure.dart';
import 'package:pet_adoption_app/core/usecase/usecase.dart';

import '../entity/pet.dart';
import '../repository/pet_adoption_repository.dart';

class AdoptPet extends UseCase<Pet, Params> {
  final PetAdoptionRepository repository;

  AdoptPet(this.repository);

  @override
  Future<Either<Failure, Pet>> call(Params params) {
    return repository.adoptPet(params.pet);
  }
}

class Params extends Equatable {
  final Pet pet;

  const Params(this.pet);

  @override
  get props => [pet];
}
