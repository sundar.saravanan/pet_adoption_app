import 'package:dartz/dartz.dart';

import '../../../../core/error/failure.dart';
import '../entity/pet.dart';

abstract class PetAdoptionRepository {
  Future<Either<Failure, List<Pet>>> getPetsList();
  Future<Either<Failure, Pet>> adoptPet(Pet pet);
}
