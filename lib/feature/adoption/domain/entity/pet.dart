// TODO: Make abstract and extend for Dog, Cat, etc
import 'package:equatable/equatable.dart';

class Pet extends Equatable {
  final String id;
  final String name;
  final int age;
  final double price;

  // TODO: Add a list of images
  // final String imageUrl;

  const Pet(this.id, this.name, this.age, this.price);

  @override
  get props => [id, name, age, price];
}
