import 'dart:convert';

import 'package:pet_adoption_app/core/error/exception.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../model/pet_model.dart';

abstract class PetsLocalDataSource {
  Future<List<PetModel>> getAdoptedPets();
  Future<List<PetModel>> getUnadoptedPets();

  Future<void> writeAdoptedPets(List<PetModel> petsList);
  Future<void> writeUnadoptedPets(List<PetModel> petsList);
}

class PetsLocalDataSourceImpl implements PetsLocalDataSource {
  SharedPreferences? _sharedPrefs;
  Future<SharedPreferences> get sharedPrefs async {
    _sharedPrefs ??= await SharedPreferences.getInstance();
    return _sharedPrefs!;
  }

  PetsLocalDataSourceImpl({SharedPreferences? sharedPrefs})
      : _sharedPrefs = sharedPrefs;

  static const adoptedPetsKey = 'ADOPTED_PETS';
  static const unadoptedPetsKey = 'UNADOPTED_PETS';

  @override
  Future<List<PetModel>> getAdoptedPets() async {
    final petsMap = await _getMapFromCache(adoptedPetsKey);
    return petsMap.values.toList();
  }

  @override
  Future<List<PetModel>> getUnadoptedPets() async {
    final petsMap = await _getMapFromCache(unadoptedPetsKey);
    return petsMap.values.toList();
  }

  @override
  Future<void> writeAdoptedPets(List<PetModel> petsList) {
    return _writeCache(adoptedPetsKey, petsList);
  }

  @override
  Future<void> writeUnadoptedPets(List<PetModel> petsList) {
    return _writeCache(unadoptedPetsKey, petsList);
  }

  Future<Map<String, PetModel>> _getMapFromCache(String key) async {
    try {
      final jsonString = (await sharedPrefs).getString(key);
      print('Cache in key:`$key` - `$jsonString`');

      if (jsonString == null || jsonString.isEmpty) return {};
      final Map<String, dynamic> jsonMap = jsonDecode(jsonString);

      final petsMap = jsonMap
          .map((id, petMap) => MapEntry(id, PetModel.fromMap(id, petMap)));

      return petsMap;
    } catch (e) {
      print('Exception while reading cache for key:`$key`, $e');
      throw CacheException();
    }
  }

  Future<void> _writeCache(String cacheKey, List<PetModel> petsList) async {
    final Map<String, dynamic> map = {};
    for (PetModel pet in petsList) {
      map[pet.id] = pet.toMap();
    }

    final jsonString = jsonEncode(map);
    final success = await (await sharedPrefs).setString(cacheKey, jsonString);

    if (!success) throw CacheException();
  }
}
