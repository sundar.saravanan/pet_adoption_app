import 'package:dartz/dartz.dart';
import 'package:pet_adoption_app/core/error/exception.dart';
import 'package:pet_adoption_app/feature/adoption/domain/entity/pet.dart';
import 'package:pet_adoption_app/core/error/failure.dart';

import '../../domain/repository/pet_adoption_repository.dart';
import '../datasource/pets_local_data_source.dart';

class PetAdoptionRepositoryImpl extends PetAdoptionRepository {
  final PetsLocalDataSource localDataSource;

  PetAdoptionRepositoryImpl(this.localDataSource);

  @override
  Future<Either<Failure, Pet>> adoptPet(Pet pet) async {
    try {
      final adoptedPets = await localDataSource.getAdoptedPets();
      final unadoptedPets = await localDataSource.getUnadoptedPets();

      // TODO: Check if pet exist and not already adopted

      final adoptedPet = unadoptedPets.firstWhere((el) => el.id == pet.id);
      unadoptedPets.removeWhere((el) => el.id == pet.id);
      adoptedPets.add(adoptedPet);

      await localDataSource.writeAdoptedPets(adoptedPets);
      await localDataSource.writeUnadoptedPets(unadoptedPets);

      return Right(pet);
    } on CacheException {
      return Left(CacheFailure());
    }
  }

  @override
  Future<Either<Failure, List<Pet>>> getPetsList() async {
    try {
      final adoptedPets = await localDataSource.getAdoptedPets();
      final unadoptedPets = await localDataSource.getUnadoptedPets();

      return Right([...adoptedPets, ...unadoptedPets]);
    } on CacheException {
      return Left(CacheFailure());
    }
  }
}
