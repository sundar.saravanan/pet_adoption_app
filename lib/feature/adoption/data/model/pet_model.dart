import '../../domain/entity/pet.dart';

class PetModel extends Pet {
  const PetModel(super.id, super.name, super.age, super.price);

  static const _nameKey = 'name';
  static const _ageKey = 'age';
  static const _priceKey = 'price';

  factory PetModel.fromMap(String id, Map<String, dynamic> map) {
    return PetModel(id, map[_nameKey], map[_ageKey], map[_priceKey]);
  }

  Map<String, dynamic> toMap() {
    return {
      _nameKey: name,
      _ageKey: age,
      _priceKey: price,
    };
  }
}
