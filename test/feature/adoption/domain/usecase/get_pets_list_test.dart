import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:pet_adoption_app/core/usecase/usecase.dart';
import 'package:pet_adoption_app/feature/adoption/domain/entity/pet.dart';
import 'package:pet_adoption_app/feature/adoption/domain/repository/pet_adoption_repository.dart';
import 'package:pet_adoption_app/feature/adoption/domain/usecase/get_pets_list.dart';

@GenerateNiceMocks([MockSpec<PetAdoptionRepository>()])
import 'get_pets_list_test.mocks.dart';

void main() {
  late GetPetsList usecase;
  late MockPetAdoptionRepository mockPetAdoptionRepository;

  setUp(() {
    mockPetAdoptionRepository = MockPetAdoptionRepository();
    usecase = GetPetsList(mockPetAdoptionRepository);
  });

  final tPetsList = [const Pet('1', 'tName', 2, 100.0)];

  test(
    'should get pets list from the repository',
    () async {
      when(mockPetAdoptionRepository.getPetsList())
          .thenAnswer((_) async => Right(tPetsList));

      final result = await usecase(NoParams());
      expect(result.isRight(), true);

      final petsList = result.getOrElse(() => throw Exception());
      expect(petsList, equals(tPetsList));

      verify(mockPetAdoptionRepository.getPetsList());
      verifyNoMoreInteractions(mockPetAdoptionRepository);
    },
  );
}
