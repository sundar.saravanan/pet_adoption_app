import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:pet_adoption_app/feature/adoption/domain/entity/pet.dart';
import 'package:pet_adoption_app/feature/adoption/domain/repository/pet_adoption_repository.dart';
import 'package:pet_adoption_app/feature/adoption/domain/usecase/adopt_pet.dart';

@GenerateNiceMocks([MockSpec<PetAdoptionRepository>()])
import 'adopt_pet_test.mocks.dart';

void main() {
  late AdoptPet usecase;
  late MockPetAdoptionRepository mockPetAdoptionRepository;

  setUp(() {
    mockPetAdoptionRepository = MockPetAdoptionRepository();
    usecase = AdoptPet(mockPetAdoptionRepository);
  });

  const tPet = Pet('1', 'tName', 2, 100.0);

  test(
    'should adopt the given pet through the repository',
    () async {
      when(mockPetAdoptionRepository.adoptPet(any))
          .thenAnswer((_) async => const Right(tPet));

      final result = await usecase(const Params(tPet));
      expect(result.isRight(), true);

      final adoptedPet = result.getOrElse(() => throw Exception());
      expect(adoptedPet, equals(tPet));

      verify(mockPetAdoptionRepository.adoptPet(tPet));
      verifyNoMoreInteractions(mockPetAdoptionRepository);
    },
  );
}
